# ZSH Configuration

ZSH configuration (fizzy-compliant).

## Documentation

See [wiki](https://gitlab.com/fizzycfg/official-cfg/configs-zsh/wikis/home)

## Contributing

See [CONTRIBUTING](./CONTRIBUTING.md)

## License

See [LICENSE](./LICENSE)
